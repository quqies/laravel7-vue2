const mix = require('laravel-mix');

mix.webpackConfig(
    webpack => {
        return {
            resolve: {
                extensions: ['.js', '.vue'],
                alias: {
                    '@': __dirname + '/resources',
                    '@root': __dirname,
                    '@pages': __dirname + '/resources/js/pages'
                },
                fallback: { "path": false },
            },
        }
    },
);

mix.webpackConfig({
    devServer: {
        proxy: {
            '*': 'http://localhost:8000'
        }
    },
});
mix.options({
    hmrOptions: {
        host: 'localhost',
        port: 8001
    }
});
mix.disableSuccessNotifications();

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js').vue()
    .sass('resources/sass/app.scss', 'public/css');

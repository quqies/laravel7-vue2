import Axios from "axios";

var token = ''

if (window.localStorage.getItem('token') != null) {
    token = JSON.parse(window.localStorage.getItem('token')).api_token;
}

export default Axios.create({
    baseURL: process.env.MIX_APP_API_URL,
    headers: {
        Authorization: 'Bearer ' + token,
    }
});

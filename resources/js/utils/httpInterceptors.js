import httpClient from "./httpClient";
import Auth from "../plugins/auth";
const actionScope = `loader`;

export function setupInterceptors({ dispatch, commit }) {
    let requestsPending = 0;
    const req = {
        pending: () => {
            requestsPending++;
            dispatch(`${actionScope}/show`);
        },
        done: () => {
            requestsPending--;
            if (requestsPending <= 0) {
                dispatch(`${actionScope}/hide`);
            }
        }
    };
    const snack = {
        setErrorSnack: (error) => {
            commit('setSnack', {
                text: error.response.data.message,
                y: 'top',
                x: 'right',
                color: 'error'
            })
            if (error.response.data.errors) {
                Object.values(error.response.data.errors).forEach(val => {
                    val.forEach(function (i, k) {
                        commit('setSnack', {
                            text: i,
                            y: 'top',
                            x: 'right',
                            color: 'error'
                        });
                    })
                })
            }
            // if (error.response.status == 401) {
            //     Auth.logout(this);
            // }
        },
        setSuccessSnack: (data) => {
            commit('setSnack', {
                text: data.message,
                y: 'top',
                x: 'right',
                color: 'success'
            })
        },
        setWarningSnack: (data) => {
            commit('setSnack', {
                text: data.message,
                y: 'top',
                x: 'right',
                color: 'warning'
            })
        }
    };
    httpClient.interceptors.request.use(
        config => {
            req.pending();
            return config;
        },
        error => {
            requestsPending--;
            req.done();
            return Promise.reject(error);
        }
    );
    httpClient.interceptors.response.use(
        response => {
            if (response.config.method != 'get') {
                if (response.data.status != 'success') {
                    snack.setWarningSnack(response.data);
                } else {
                    snack.setSuccessSnack(response.data);
                }
            }
            req.done();
            return Promise.resolve(response.data);
        },
        error => {
            snack.setErrorSnack(error);
            req.done();
            return Promise.reject(error);
        }
    );
}



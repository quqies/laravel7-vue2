/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Vue from "vue";
import Vuetify from "../plugins/vuetify";

import "material-design-icons-iconfont/dist/material-design-icons.css";

import Routes from "@/js/routes.js";
import App from "@/js/views/App";

import store from "@/js/stores/index";

import httpClient from "./utils/httpClient";
import { setupInterceptors } from "./utils/httpInterceptors";
import helpers from "@/js/plugins/helpers";

const plugin = {
    install() {
        Vue.helpers = helpers;
        Vue.prototype.$helpers = helpers;
    },
};

Vue.prototype.$http = httpClient;

Vue.use(plugin);

Vue.use(require("vue-moment"));

Vue.mixin({
    methods: {
        pluck: function (array, key) {
            return array.map(function (obj) {
                return obj[key];
            });
        },
        hasPrivilege: function (key) {
            return this.$store.state.userData.privilege.includes(key);
        },
        hasMenu: function (key) {
            return this.$store.state.userData.menus.includes(key);
        },
    },
});
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const app = new Vue({
    el: "#app",
    vuetify: Vuetify,
    store,
    created() {
        setupInterceptors(store);
    },
    router: Routes,
    render: (h) => h(App),
});

export default app;


import Vue from "vue";
import Vuex from "vuex";

import { loader } from './modules/loader';

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        user: {
            username: '',
            fullname: ''
        },
        isAuthenticate: false,
        snack: {
            text: '',
            y: 'top',
            x: null,
            timeout: 6000,
            color: '',
            snackbar: false
        },
        userData: JSON.parse(window.localStorage.getItem('userData')),
        token: JSON.parse(window.localStorage.getItem('token')),
        drawerState: false,
        userViewedRole: '',
        appSettings: JSON.parse(window.localStorage.getItem('appSettings'))
    },
    modules: {
        loader
    },
    getters: {
        drawerState: (state) => state.drawerState
    },
    mutations: {
        authenticate(state, cond) {
            state.isAuthenticate = cond.isAuthenticate
            state.token = cond.token
        },
        setSnack(state, snack) {
            state.snack.text = snack.text;
            state.snack.y = snack.y == undefined ? state.snack.y : snack.y;
            state.snack.x = snack.x == undefined ? state.snack.x : snack.x;
            state.snack.timeout = snack.timeout == undefined ? state.snack.timeout : snack.timeout;
            state.snack.color = snack.color == undefined ? state.snack.color : snack.color;
            state.snack.snackbar = true;
        },
        toggleDrawer(state, data) {
            state.drawerState = data
        },
        setUserViewedRole(state, data) {
            state.userViewedRole = data
        },
    },
    actions: {}
});

import Axios from "axios";
import router from "../routes";
import store from "../stores/index";

export default {
    authenticated: false,
    user: "",
    role: "",
    getToken() {
        let token = window.localStorage.getItem("token");
        if (token == undefined || token == null) {
            return "";
        }
        return JSON.parse(token).access_token;
    },
    login(context, creds) {
        context.$http.post("auth/login", creds).then(
            (response) => {
                if (response.status == 400) {
                    this.authenticated = false;
                    return router.go({ name: "home" });
                }
                window.localStorage.setItem(
                    "token",
                    JSON.stringify(response.data)
                );

                setTimeout(function () {
                    context.$http
                        .get("me", {
                            headers: {
                                Authorization: `Bearer ${response.data.api_token}`,
                            },
                        })
                        .then((response) => {
                            window.localStorage.setItem(
                                "userData",
                                JSON.stringify(response.data)
                            );

                            this.authenticated = true;

                            location.reload();
                        });
                }, 1000);
            },
            (errors) => {
                context.errors = errors;
            }
        );
    },
    checkAuth: function () {
        if (window.localStorage.getItem("token")) {
            return true;
        } else {
            return false;
        }
    },
    logout: function (context) {
        this.authenticated = false;
        window.localStorage.removeItem("app");
        window.localStorage.removeItem("token");
        window.localStorage.removeItem("userData");
        window.location.reload();
        // return context.$router.go({ name: 'login' })
    },
};

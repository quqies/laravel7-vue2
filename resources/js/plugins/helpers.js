export default {
    capFirstLetter(val) {
        return val.charAt(0).toUpperCase() + val.slice(1);
    },
    pluck: function (array, key) {
        return array.map(function (obj) {
            return obj[key];
        });
    },
    getPathFromUrl: function () {
        return window.location.href.split(/[?#]/)[0];
    },
    isHaveRole: function (role)
    {
        return JSON.parse(window.localStorage.getItem('userData')).roles.includes(role);
    }
};

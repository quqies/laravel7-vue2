import Vue from 'vue'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

const opts = {
    theme: {
        themes: {
            light: {
                appBlue: '#1FB6FF',
            },
        },
    },
}

export default new Vuetify(opts)

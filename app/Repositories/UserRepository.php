<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Carbon;

class UserRepository
{
    public function findUserByUsername(string $username)
    {
        return User::where('username', $username)->first();
    }

    public function generateToken(User $user)
    {
        $token = hash('sha512', $user->nip . $user->id . time());
        $user->api_token = $token;
        $user->last_login_at = Carbon::now();
        $user->last_active_at = Carbon::now();
        $user->saveOrFail();
        return $token;
    }

    public function findPaginated($perPage, $filter)
    {
        return $this->queryFind($filter)->paginate($perPage);
    }

    public function queryFind($filter)
    {
        $user = User::query();
        if (!empty($filter['search'])) {
            $search = '%' . $filter['search'] . '%';
            $user->where(function ($query) use ($search) {
                $query->where('full_name', 'like', $search)
                    ->orWhere('email', 'like', $search)
                    ->orWhere('username', 'like', $search);
            });
        }
        return $user;
    }
}

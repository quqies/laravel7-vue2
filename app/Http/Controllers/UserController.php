<?php

namespace App\Http\Controllers;

use App\Http\Resources\UserResource;
use App\Models\User;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function create(Request $request, UserRepository $userRepository)
    {
        $request->validate([
            'username' => [
                'required',
                Rule::unique(User::class, 'username')
            ],
            'full_name' => [
                'required'
            ],
            'email' => [
                'required'
            ],
            'password' => [
                'required'
            ]
        ]);

        $userData = [
            'username' => $request->input('username'),
            'full_name' => $request->input('full_name'),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'status' => 'actived'
        ];

        $user = User::create($userData);

        return [
            'status' => 'success',
            'message' => 'User created manually',
            'data' => [
                'id' => $user->id
            ]
        ];
    }

    public function index(Request $request, UserRepository $userRepository)
    {
        $perPage = $request->input('perPage', 25);
        return UserResource::collection($userRepository->findPaginated($perPage, $request->all()));
    }

    public function delete(User $user)
    {
        $user->delete();
        return [
            'status' => 'success',
            'message' => 'User Deleted',
        ];
    }
}

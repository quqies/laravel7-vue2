<?php

namespace App\Http\Controllers;

use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request, UserRepository $userRepository)
    {
        if (Auth::attempt(['username' => $request->username, 'password' => $request->password])) {
            $user = $userRepository->findUserByUsername($request->username);
            if (!empty($user)) {
                if ($user->status == 'disabled') {
                    return response()->json([
                        'status' => 'error',
                        'message' => __('User Kamu Belum Terverifikasi'),
                    ], 400);
                }
            }
            $token = $userRepository->generateToken($user);
            return [
                'status' => 'success',
                'message' => __('Authenticated'),
                'data' => [
                    'api_token' => $token,
                ]
            ];
        }

        return response()->json([
            'status' => 'error',
            'message' => __('User/Password Salah'),
        ], 400);
    }
}

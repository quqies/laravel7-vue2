<?php

namespace App\Http\Middleware;

use Closure;

class ConvertNullStringToNull
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all();
        foreach ($input as $key => &$req) {
            $req = $req !== 'null' ? $req : null;
        };
        $request->replace($input);
        return $next($request);
    }
}

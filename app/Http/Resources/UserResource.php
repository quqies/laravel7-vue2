<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'username' => $this->resource->username,
            'full_name' => $this->resource->full_name,
            'email' => $this->resource->email,
            'avatar' => $this->resource->avatar,
            'created_at' => $this->resource->created_at,
        ];
    }
}

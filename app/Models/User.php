<?php

namespace App\Models;

use App\Models\Traits\AutoGenerateUuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use AutoGenerateUuid;
    use SoftDeletes;

    protected $connection = 'mysql';
    protected $guarded = [];
    protected $table = 'users';
    protected $primaryKey = 'id';
}
